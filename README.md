## C++

These notes are taken by using Emacs org-mode with the ability to execute source code directly within org-mode documents.

The content is collected from Internet, books.

## References
[Bo Qian - Youtube Channel](https://www.youtube.com/channel/UCEOGtxYTB6vo6MQ-WQ9W_nQ)

## Note
Because bitbucket does not suport org-mode viewer, this repository is moved to [github](https://github.com/balx/programming_languages)
